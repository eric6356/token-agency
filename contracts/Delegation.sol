pragma solidity ^0.4.24;


contract Delegation {
    address public client;
    address public agent;
    bool public isRevoked;

    modifier onlyClient {
        require(msg.sender == client);
        _;
    }

    modifier onlyAgent {
        require(msg.sender == agent);
        _;
    }

    function revoke() public onlyClient {
        isRevoked = true;
    }
}


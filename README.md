# Token Agency

## Overview
If you have some ethereum or [token](https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20.md) and you want to trade them for profit, but you don't know(and probrally you don't want to know either) what's the best time to sell or buy, you'd better find an agency to trade on behalf of you.  
This project is smart contract solution to such token trade agent problem.

## Participant
### Client
Someone who owns ethereum or token and wants to make profit from it.

### Agency
The person or organization who authorized by client and actually does the trade.

## Workflow
### Delegation Contract Sign
Client and Agency sign a smart contract specifying the the token(s) to sell or buy, how the interest is shared, validity date, etc. This contract authorizes the agent to have right to trade client's tokens under certain conditions.

### Delegated Trade
On behalf of client, agency can buy or sell tokens and interests will be shared as per contract.

### Authorization Revoke (Optional)
Client can revoke authorization at any time.

## Examples
### MinimumInterestDelegation
> TBD

### LeverageDelegation
> TBD
